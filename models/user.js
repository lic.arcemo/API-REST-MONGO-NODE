let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let UserSchema = new Schema({
  first_name: {
    type: String,
    required: 'Por favor escribe tu nombre'
  },
  last_name: {
    type: String,
      required: 'Por favor escribe tu apellido.'
  },
  email: {
    type: String,
    index: {
      unique: true
    },
    required: 'Por favor escribe tu email.'
  },
  password: {
    type: String,
    required: 'Por favor escribe tu contraseña.'
  },
  activities: [{
    type: Schema.Types.ObjectId,
    ref: 'Todo'
  }]
}, {
  timestamps: true, // adds createdAt and updatedAt fields automatically
  minimize: false   // will make sure all properties exist, even if null
});
UserSchema.index({ last_name: 1, first_name: 1 });
module.exports = mongoose.model('User', UserSchema);
