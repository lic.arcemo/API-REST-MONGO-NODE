let mongoose  = require('mongoose');
let Schema    = mongoose.Schema;
let TodoSchema = new Schema({
  actor: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: 'Es necesario el actor'
  },
  start: {
    type: Date,
    default: Date.toUTCString
  },
  end: {
    type: Date,
    default: Date.toUTCString
  },
  description: {
    type: String,
    required: 'Por favor escribe la tarea',
    generator: 'lorem.sentence'
  },
  ready: {
    type: Number,
    required: 'Por favor escribe el status de la actividad'
  }
}, {
  timestamps: true, // adds createdAt and updatedAt fields automatically
});
TodoSchema.index({ actor: 1, createdAt: -1 });
module.exports = mongoose.model('Todo', TodoSchema);
