var jwt = require('jsonwebtoken');
let User = require('../models/user');
let Auth = require('../middleware/auth');

var bcrypt = require('bcrypt');

module.exports = {
  login: ( req, res, next ) => {
        User.find({email: req.body.email }, (err, user) => {
          if (err) {
            return next(err);
          }
          else{

            if(user.length>0){
              bcrypt.compare(req.body.password, user[0].password, function(err, ok) {
                console.log(ok);
                if( ok ){
                  var token = jwt.sign({ id: user._id }, process.env.AUTH0_SECRET, {
                    expiresIn: 86400 // expires in 24 hours
                  });
                  res.json({ auth: true, token: token, user_id: user[0]._id, first_name: user[0].first_name, last_name: user[0].last_name   });
                }
                else{
                  res.json({ auth: false, token: '', user_id: '', message:"Email y/o contraseña incorrecta."  });
                }
              });
            }
            else{
              res.json({ auth: false, token: '', user_id: '', message:"Usuario no encontrado." });
            }
          }

        });
  }
}
