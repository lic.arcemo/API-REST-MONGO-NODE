var jwt = require('jsonwebtoken');
let User = require('../models/user');
let Auth = require('../middleware/auth');

var bcrypt = require('bcrypt');
const saltRounds = 10;
const myPlaintextPassword = 's0/\/\P4$$w0rD';
const someOtherPlaintextPassword = 'not_bacon';


module.exports = {
  getAll: ( req, res, next ) => {

  Auth.checkAuth(req.headers['x-access-token'],function(data){
    if (data.auth){
      User.find(req.query, (err, users) => {
        if (err) {
          return next(err);
        }
        res.json( users );
      });
    }
    else{
      res.json( data );
    }
  });

  },
  create: (req, res, next) => {
    let new_user = new User(req.body);

    bcrypt.genSalt(saltRounds, function(err, salt) {
    bcrypt.hash(req.body.password, salt, function(err, hash) {
          new_user.password = hash;
          new_user.save((err, user) => {
            if (err) {
              return next(err);
            }
            var token = jwt.sign({ id: user._id }, process.env.AUTH0_SECRET, {
              expiresIn: 86400 // expires in 24 hours
            });
            res.json({ auth: true, token: token, user_id: user._id  });
          });
        });
    });

  },
  getOne: (req, res, next) => {
    User.findById(req.params.id, (err, user) => {
      if (err) {
        return next(err);
      }
      res.json(user);
    });
  },
  update: (req, res, next) => {
    User.findOneAndUpdate({ _id: req.params.id }, req.body, {new: true, runValidators: true}, (err, user) => {
      if (err) {
        return next(err);
      }
      res.json(user);
    });
  },
  delete: (req, res, next) => {
    User.findOneAndRemove({ _id: req.params.id }, (err, user) => {
      if (err) {
        return next(err);
      }
      res.json(user);
    });
  }
};
