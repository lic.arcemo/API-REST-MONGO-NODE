let Activity = require('../models/activity');
let Auth = require('../middleware/auth');

module.exports = {
  getAll: (req, res, next) => {

    Auth.checkAuth(req.headers['x-access-token'],function(data){
      if (data.auth){
        Activity.find(req.query, (err, activities) => {
          if (err) {
            return next(err);
          }
          res.json(activities);
        });
      }
      else{
        res.json( data );
      }
    });
  },
  create: (req, res, next) => {

    Auth.checkAuth(req.headers['x-access-token'],function(data){
      if (data.auth){
        let new_activity = new Activity(req.body);
        new_activity.save((err, activity) => {
          if (err) {
            return next(err);
          }
          res.json(activity);
        });
      }
      else{
        res.json( data );
      }
    });
  },
  getOne: (req, res, next) => {

    Auth.checkAuth(req.headers['x-access-token'],function(data){
      if (data.auth){
        Activity.findById(req.params.id, (err, activity) => {
          if (err) {
            return next(err);
          }
          res.json(activity);
        });
      }
      else{
        res.json( data );
      }
    });

  },
  update: (req, res, next) => {

    Auth.checkAuth(req.headers['x-access-token'],function(data){
      if (data.auth){
        Activity.findOneAndUpdate({ _id: req.params.id }, req.body, {new: true, runValidators: true}, (err, activity) => {
          if (err) {
            return next(err);
          }
          res.json(activity);
        });
      }
      else{
        res.json( data );
      }
    });

  },
  delete: (req, res, next) => {

    Auth.checkAuth(req.headers['x-access-token'],function(data){
      if (data.auth){
        Activity.findOneAndRemove({ _id: req.params.id }, (err, activity) => {
          if (err) {
            return next(err);
          }
          res.json(activity);
        });
      }
      else{
        res.json( data );
      }
    });
  }
};
