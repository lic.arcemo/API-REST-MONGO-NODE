
var jwt = require('jsonwebtoken');

module.exports = {
  checkAuth: ( token, callback ) => {

      if ( !token ) {
        return callback(  { auth: false, message: 'No existe el token' });
      }
      else {
        jwt.verify( token, process.env.AUTH0_SECRET, function( err, decoded) {

          if (err) {
            return callback({ auth: false, message: 'Token inválido.' });
          }
          else {
            return callback({ auth: true, message: 'ok' })
          }
        });
      }
  }

}
