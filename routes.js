'use strict';

// import controllers
let User = require('./controllers/user');
let Activity = require('./controllers/activity');
let Auth = require('./controllers/login');

// export route generating function
module.exports = app => {

  // "Hello, World!" route
  app.route('/test').get((req, res) => {
    res.json({
      message: 'Token is  REST API'
    });
  });

  app.route('/').get((req, res) => {
    res.json({
      message: `This is the ${process.env.APP} REST API`
    });
  });

  app.route('/users')
    .get(User.getAll)
    .post(User.create);

  app.route('/users/login')
    .post(Auth.login)

  app.route('/users/:id')
    .get(User.getOne)
    .post(User.update)
    .delete(User.delete);

  app.route('/activities')
    .get(Activity.getAll)
    .post(Activity.create);

  app.route('/activities/:id')
    .get(Activity.getOne)
    .post(Activity.update)
    .delete(Activity.delete);

};
