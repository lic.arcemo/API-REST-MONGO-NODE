let app = require('./server');

// start server
app.listen(8080, () => {
  console.log('API listening on port 8080');
});
